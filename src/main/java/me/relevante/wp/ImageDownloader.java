package me.relevante.wp;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class ImageDownloader {

    private static String SOURCE_IMAGE_NAME = "Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg";
    private static String SOURCE_IMAGE_URL = "http://www.jqueryscript.net/images/" + SOURCE_IMAGE_NAME;
    private static String WP_MEDIA_ENDPOINT = "http://cybersec.hub.relevante.me/wp-json/wp/v2/media";

    public static void main(String[] args) throws IOException {

        CloseableHttpClient client = HttpClients.createDefault();

        CloseableHttpResponse resp =  client.execute(new HttpGet(SOURCE_IMAGE_URL));
        byte[] bytes = IOUtils.toByteArray(resp.getEntity().getContent());

        resp.close();

        HttpPost req = buildUploadImageReq(bytes);

        client.execute(req);

        client.close();

    }

    private static HttpPost buildUploadImageReq(byte[] bytes) throws IOException {
        HttpPost req = new HttpPost(WP_MEDIA_ENDPOINT);

        req.setEntity(new ByteArrayEntity(bytes));
        req.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
        req.addHeader("Content-Disposition", "attachment; filename=\"" + SOURCE_IMAGE_NAME + "\"");
        req.addHeader(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46Rzd0TCBzWVE0IDVwR2cgNm5UVSB0RGNSIDhrdnQ=");
        return req;
    }

}
